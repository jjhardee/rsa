﻿using System;
using System.Numerics;

namespace RSA
{
	public class RSA
	{
		private PublicKey publicKey;
		private long privateKey;

		public RSA(int bits)
		{
			// generate public, private key pair
			long p = MathHelper.RandomPrime(bits, 256);
			long q = MathHelper.RandomPrime(bits, 256);
			long t = (p - 1) * (q - 1);

			publicKey = new PublicKey(p * q, MathHelper.RandomPrime(t, 256));
			privateKey = MathHelper.Invert(publicKey.exponent, t);
		}

		public long Encrpyt(long m)
		{
			return (long) BigInteger.ModPow(m, publicKey.exponent, publicKey.modulus);
		}

		public long Decrpyt(long mEnc)
		{
			return (long)BigInteger.ModPow(mEnc, privateKey, publicKey.modulus);
		}
	}

	public struct PublicKey
	{
		public long modulus, exponent;

		public PublicKey(long m, long e)
		{
			modulus = m;
			exponent = e;
		}
	}

	public static class MathHelper
	{
		public static long RandomPrime(int bits, long accuracy)
		{
			Random rand = new Random();
			bool finished = false;
			long p = 0;

			while (!finished)
			{
				p = RandomLong(1L << (bits - 1), 1L << bits, rand);
				finished |= ProbablyPrime(p, accuracy);
			}

			return p;
		}

		// max exclusive
		public static long RandomPrime(long max, long accuracy)
		{
			Random rand = new Random();
			bool finished = false;
			long p = 0;

			while (!finished)
			{
				p = RandomLong(1, max, rand);
				finished |= ProbablyPrime(p, accuracy);
			}

			return p;
		}

		// max exlusive
		public static long RandomLong(long min, long max, Random rand)
		{
			byte[] buf = new byte[8];
			rand.NextBytes(buf);
			long longRand = BitConverter.ToInt64(buf, 0);

			return (Math.Abs(longRand % (max - min)) + min);
		}

		/* n > 3 */
		public static bool ProbablyPrime(long n, long accuracy)
		{
			if (n == 0 || n == 1) return false;
			if (n == 2 || n == 3) return true;

			Random rand = new Random();
			long d = n - 1;
			long r = 0;
			bool finished = false;
			for (; d % 2 == 0; d /= 2, r++)
			{
			}


			for (int i = 0; i < accuracy; i++)
			{
				long a = RandomLong(2, n - 1, rand);
				long x = (long)BigInteger.ModPow(a, d, n);

				if (x == 1 || x == n - 1)
				{
					continue;
				}

				finished = true;
				for (int j = 0; j < r - 1; j++)
				{
					x = (long)BigInteger.ModPow(x, x, n);
					if (x == 1)
					{
						return false;
					}
					if (x == n - 1)
					{
						finished = false;
						break;
					}
				}
				if (finished)
				{
					return false;
				}
			}
			return true;
		}

		/* return a^{-1} mod n */
		public static long Invert(long a, long n)
		{
			/*
			 * t  (1) r
			 * nt (0) nr
			 */
			long t = 0;
			long nt = 1;
			long r = n;
			long nr = a % n;
			long modulus = n;
			while (nr != 0)
			{
				long quot = r / nr;
				long tmp = nt;
				nt = t - quot * nt;
				t = tmp;
				tmp = nr; 
				nr = r - quot * nr; 
				r = tmp;
			}
			if (t < 0)
			{
				t += modulus;
			}

			return t;
		}
	}
}
