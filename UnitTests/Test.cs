﻿using NUnit.Framework;
using System;

namespace UnitTests
{
	[TestFixture()]
	public class MathHelperTest
	{
		[Test()]
		public void TestInvert()
		{
			Assert.AreEqual(RSA.MathHelper.Invert(8, 11), 7);
		}

		[Test()]
		public void TestProbablyPrime()
		{
			Assert.IsTrue(RSA.MathHelper.ProbablyPrime(7919, 256));
			Assert.IsFalse(RSA.MathHelper.ProbablyPrime(7920, 256));
		}

		[Test()]
		public void TestRandomPrimeByBits()
		{
			for (int i = 0; i < 10; i++)
			{
				Assert.IsTrue(RSA.MathHelper.ProbablyPrime(RSA.MathHelper.RandomPrime((int)16, 256), 256));
			}
		}

		[Test()]
		public void TestRandomPrimeByMax()
		{
			for (int i = 1; i < 10; i++)
			{
				Assert.IsTrue(RSA.MathHelper.ProbablyPrime(RSA.MathHelper.RandomPrime((long)10000 * i, 256), 256));
			}
		}
	}

	[TestFixture()]
	public class RSATest
	{
		RSA.RSA cipher;

		[SetUp]
		public void CipherCreation()
		{
			cipher = new RSA.RSA(16);
		}

		[Test()]
		public void TestEncrypt()
		{
			Random rand = new Random();
			for (int i = 0; i < 1000; i++)
			{
				long m = RSA.MathHelper.RandomLong(1, 1 << 16, rand);
				Assert.AreEqual(m, cipher.Decrpyt(cipher.Encrpyt(m)));
			}
		}
	}
}